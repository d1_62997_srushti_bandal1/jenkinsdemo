const express = require('express')

const app = express()

app.get('/', (request, response) => {
  console.log(
    'Server will be closed 10 seconds after receiving the first request '
  )
  console.log('Request received ')

  response.send('hello from server')
  setTimeout(() => {
    process.exit()
  }, 10000)
})

app.listen(8000, '0.0.0.0', () => {
  console.log('Server started on port 8000')
})
